class CloudUploadMailer < ApplicationMailer

  def upload_file(user,page,u_file,download_url,cc_emails = '')
	  @user = user
    @page = page
    @uploaded_file = u_file
    @download_url = download_url
    mail(to: @user.email, subject: "Uploaded file on #{@page.cloud_storage_type}",:cc => cc_emails)
	end
end
