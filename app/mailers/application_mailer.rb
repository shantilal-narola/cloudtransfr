class ApplicationMailer < ActionMailer::Base
  # default from: 'hello@cloudtransfr.com'
  default from: 'cloudttr@gmail.com'  
  layout 'mailer'
end
