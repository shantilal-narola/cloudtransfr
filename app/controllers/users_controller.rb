class UsersController < ApplicationController
  def edit
  	@user = User.find(params[:id])
  end

  def update
  	@user = User.find(params[:id])
  	@user.update(email: params[:email])
  	redirect_to edit_user_path(@user)
  end

  def check_email
    @user = User.find_by_email(params[:user][:email])
    respond_to do |format|
      format.json { render :json => {:valid => !@user} }
    end
  end

  def is_email_exist
    @user = User.find_by_email(params[:user][:email])
    @user_res = @user.present? ? true : false
    respond_to do |format|
      format.json { render :json => {:valid => @user_res} }
    end
  end

  def check_page
    @page = Page.find_by_name(params[:user][:pagename])
    respond_to do |format|
      format.json { render :json => {:valid => !@page} }
    end
  end
end
