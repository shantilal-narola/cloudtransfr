class Admins::UsersController < ApplicationController
  before_action :authenticate_admin!
  layout "admins"

  def index
    @users = User.all.includes(:pages)
  end

  def show
    @user = User.find(params[:id])
    @user_sub = @user.user_subscriptions.includes(:subscription_plan)
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:notice] = "User was updated."
      redirect_to root_path
    else
      flash[:error] = "User was not updated."
      redirect_to root_path
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "User deleted successfully."
    redirect_to root_path
  end

  def decline_request
    @decline_request = PaymentDeclineRequest.all.includes(:user,:subscription_plan)
  end

  def update_request_status
    @decline_request = PaymentDeclineRequest.find(params[:id])
    if @decline_request.update(is_approved: true)
      flash[:notice] = "Request was approved successfully."
      redirect_to admins_decline_request_path
    else
      flash[:error] = "Request was not approve successfully."
      redirect_to admins_decline_request_path
    end
  end

  def destroy_request
    @decline_request = PaymentDeclineRequest.find(params[:id])
    @decline_request.destroy
    flash[:notice] = "Request deleted successfully."
    redirect_to admins_decline_request_path
  end

  private

	def user_params
		params.require(:user).permit(:email)
	end

end
