class Admins::DocumentsController < ApplicationController
  require 'dropbox_sdk'
  require 'dropbox'
  require 'uri'
  require 'net/http'
  require 'net/https'
  require "googleauth"
  require "openssl"
  require 'open-uri'
  require 'google/apis/drive_v3'
  OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

  before_action :authenticate_admin!
  layout "admins"

  def index
    @page = Page.friendly.find(params[:id])
    @user = @page.user
    if @page.cloud_storage_type == "dropbox"
      @paths = @page.dropbox_infos.distinct
    elsif @page.cloud_storage_type == "googledrive"
      @paths = @page.google_infos.distinct
    elsif @page.cloud_storage_type == "onedrive"
      @paths = @page.onedrive_infos.distinct
    end
  end
end
