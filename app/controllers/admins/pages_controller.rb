class Admins::PagesController < ApplicationController
  require 'dropbox_sdk'
  require 'dropbox'
  require 'uri'
  require 'net/http'
  require 'net/https'
  require "googleauth"
  require "openssl"
  require 'open-uri'
  require 'google/apis/drive_v3'
	OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

  before_action :authenticate_admin!
  layout "admins"

  def index
    if params[:id].present?
      @pages = User.find(params[:id]).pages
    else
      @pages = Page.all
    end
  end

  def destroy
    @page = Page.find(params[:id])
    @previous_url = request.referrer
    puts "cloud_storage_type --> ",@page.cloud_storage_type
    if @page.cloud_storage_type == "dropbox"
      delete_dropbox_folder(@page)
    elsif @page.cloud_storage_type == "googledrive"
      delete_google_drive_folder(@page)
    elsif @page.cloud_storage_type == "onedrive"
      delete_one_drive_folder(@page)
    else
      @page.destroy
      flash[:notice] = "Page deleted successfully."
      redirect_back(fallback_location: @previous_url)
    end
  end

  def delete_dropbox_folder(page)
    @previous_url = request.referrer
    begin
      if page.present?
        if page.dropbox_infos.present?
          dbx = Dropbox::Client.new(page.user.dropbox_detail.access_token)
          dbx.delete("/Apps/CloudTransfr/#{page.name}")
        end
        page.destroy
        flash[:notice] = "Page deleted successfully."
        redirect_back(fallback_location: @previous_url)
      else
        flash[:error] = "Page is not exist."
        redirect_back(fallback_location: @previous_url)
      end
    rescue Exception => e
      puts "Error: #{e.message}"
      flash[:error] = "Error: #{e.message}"
      redirect_back(fallback_location: @previous_url)
    end
  end

  def delete_google_drive_folder(page)
    @previous_url = request.referrer
    begin
      if page.present?
        if page.google_infos.present?
          if !page.user.google_detail.refresh_token.present?
            puts "access_token expire --->"
            flash[:error] = "User google drive access token is expire. So, you can not delete user's google drive folder."
            redirect_back(fallback_location: @previous_url)
          else
            @credentials = google_drive_credential
            @credentials.refresh_token = page.user.google_detail.refresh_token
            @credentials.fetch_access_token!
            # DELETE https://www.googleapis.com/drive/v2/files/FILE_ID
            uri = URI("https://www.googleapis.com/drive/v2/files/#{page.folder_id}")

            Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
              request = Net::HTTP::Delete.new uri
              request["authorization"] = "Bearer #{@credentials.access_token}"
              response = http.request request # Net::HTTPResponse object

              flash[:notice] = "Page deleted successfully."
              page.destroy
            end
            redirect_back(fallback_location: @previous_url)
          end
        else
          page.destroy
          flash[:notice] = "Page deleted successfully."
          redirect_back(fallback_location: @previous_url)
        end
      else
        flash[:error] = "Page is not exist."
        redirect_back(fallback_location: @previous_url)
      end
    rescue Exception => e
      puts "Error: ", e.message
      flash[:error] = "Error: #{e.message}"
      redirect_back(fallback_location: @previous_url)
    end
  end

  def delete_one_drive_folder(page)
    @previous_url = request.referrer
    begin
      if page.present?
        if !page.user.onedrive_detail.refresh_token.present?
          puts "access_token expire --->"
          flash[:error] = "User one drive access token is expire. So, you can not delete user's one drive folder."
          redirect_back(fallback_location: @previous_url)
        else
          get_new_onedrive_access_token(page)
          @access_token = page.user.onedrive_detail.access_token
          # DELETE https://apis.live.net/v5.0/folder.a6b2a7e8f2515e5e.A6B2A7E8F2515E5E!114?access_token=ACCESS_TOKEN
          uri = URI("https://apis.live.net/v5.0/#{page.folder_id}?access_token=#{@access_token}")

          Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
            request = Net::HTTP::Delete.new uri
            response = http.request request # Net::HTTPResponse object
            puts "response --> ",response.inspect
            flash[:notice] = "Page deleted successfully."
            page.destroy
          end
          redirect_back(fallback_location: @previous_url)
        end
      else
        flash[:error] = "Page is not exist."
        redirect_back(fallback_location: @previous_url)
      end
    rescue Exception => e
      flash[:error] = "Error: #{e.message}"
      redirect_back(fallback_location: @previous_url)
    end
  end


end
