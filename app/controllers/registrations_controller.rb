class RegistrationsController < Devise::RegistrationsController
  # before_filter except: [:destroy,:after_sign_up_path_for]
 # before_action :create, only: [after_sign_up_path_for]

  def after_sign_up_path_for(resource)
    if resource.dropbox_detail.present?
      if params[:plan_id].present?
        addcard_path(params[:plan_id])
      else
        root_url
      end
     else
       if params[:plan_id].present?
         addcard_path(params[:plan_id])
       else
         staticpage_index_path
       end
     end
  end

  def create
    super
    begin
      WelcomeMailer.welcome(resource).deliver unless resource.invalid?
    rescue Exception => e
      puts "Mail is not send : #{e.message}"
    end
    if resource.valid?
      if params[:user][:pagename].present?
        @page = resource.pages.new
        @page.name = params[:user][:pagename]
        @page.save!
      end
    end
  end

end
