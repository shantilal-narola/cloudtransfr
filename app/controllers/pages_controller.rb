class PagesController < ApplicationController
  include PagesHelper
  require 'dropbox_sdk'
  require 'dropbox'
  require 'uri'
  require 'net/http'
	require 'net/https'
	require "googleauth"
	require "openssl"
  require 'open-uri'
  require 'google/apis/drive_v3'
	OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

	# before_action :authenticate_user!, except: [:show_page, :upload, :add_question_answer, :dropbox_download, :googledrive_download, :onedrive_download]
  before_action :authenticate_any!, except: [:show_page, :upload, :add_question_answer, :dropbox_download, :googledrive_download, :onedrive_download]
  before_action :check_payment
  before_action :file_size_validation, only: [:show_page]
  before_action :page_validation, only: [:new_page]
  before_action :decrease_total_size, only: [:destroy]

  def index
    check_free_user
    check_plan
  	@pages = current_user.pages.all

    @drop_box_pages = current_user.pages.where(cloud_storage_type: "dropbox")
    @google_drive_pages = current_user.pages.where(cloud_storage_type: "googledrive")
    @one_drive_pages = current_user.pages.where(cloud_storage_type: "onedrive")

  	unless @pages.present?
  		redirect_to new_page_path
  	end
  end

  def new
  	@pages = Page.new
  end

  def new_page
    if @allowed.present?
      @pages = Page.new
    else
      flash[:notice] = "Not allowed create another page please switch plan to create page"
      redirect_to switch_plan_path
    end
  end

  def create
  	@pages = current_user.pages.new(page_params)
  	respond_to do |format|
      if @pages.save
        if params[:page][:cloud_storage_type] == "onedrive"
          @onedrive_folder = JSON.parse(create_onedrive_folder(@pages))
          puts "@onedrive_folder --> ",@onedrive_folder.to_json
          @pages.update(folder_id: @onedrive_folder["id"])
        elsif params[:page][:cloud_storage_type] == "googledrive"
          @credentials = google_drive_credential
          @credentials.refresh_token = @pages.user.google_detail.refresh_token
          @credentials.fetch_access_token!
          @a_token =  @credentials.access_token
          @pages.user.google_detail.update(access_token: @a_token)
          session = GoogleDrive::Session.from_credentials(@credentials)

          @new_folder = session.root_collection.create_subcollection(@pages.name)
          @pages.update(folder_id: @new_folder.id)
          puts "@new_folder ---> ",@new_folder.inspect
        end

        # Add default fields of page uploader's name, email, address, phone number
        if !@pages.page_questions.present?
          @pages.page_questions.build(question: "name",question_type: "Text", description: "name", is_required: true)
          @pages.page_questions.build(question: "email",question_type: "Email", description: "email", is_required: true)
          @pages.page_questions.build(question: "address",question_type: "Long Text", description: "address", is_required: true)
          @pages.page_questions.build(question: "phone number",question_type: "Phone Number", description: "phone number", is_required: true)
          @pages.save
        end
        format.html { redirect_to pages_path, notice: 'Successfully created the url.' }
      else
        format.html { render "new" }
      end
  	end
  end

  def page_exist
    @page = Page.find_by_name(params[:page][:name])
    respond_to do |format|
      format.json { render :json => {:valid => !@page} }
    end
  end

  def edit
    @question_type = ["Text", "Email", "Phone Number", "URL", "Number", "Checkbox", "Date", "Date with Time", "Long Text"]
    @page = Page.find(params[:id])
    @page_que = @page.page_questions.new
  end

  def update
    @page = Page.find(params[:id])
  	@page.update(page_params)

    if params[:cc_emails].present?
      if @page.page_cc_email.present?
         @page.page_cc_email.update_attributes(:cc_email => params[:cc_emails])
      else
        page_cc = @page.create_page_cc_email(:cc_email => params[:cc_emails])
        page_cc.save
      end
    end

    if params[:page][:question].present? && params[:page][:question_type].present? && params[:page][:description].present?
      is_required = params[:page][:is_required].present? && params[:page][:is_required] == "on" ? true : false
      @page.page_questions.build(question: params[:page][:question],question_type: params[:page][:question_type], description: params[:page][:description], is_required: is_required)
      @page.save
    end

  	redirect_to pages_path
  end

  def add_question
    @page = Page.find(params[:id])
    @page_upload = @page.page_questions.build(question: params[:question], question_type: params[:question_type], description: params[:description], is_required: params[:is_required])
    if @page_upload.save
        render json: {message: "Add setting",id: @page_upload.id}
    else
      render json: {message: "Error"}
    end
  end

  def remove_question
    @page_question = PageQuestion.find(params[:id])
    if @page_question.present?
      @page_question.delete
      render json: {message: "Question deleted" }
    else
      render json: {message: "Question not exist" }
    end
  end

  def add_question_answer
    @page = Page.friendly.find(params[:page_id])
    if @page.present?
      cookies["#{@page.name}"] = params[:question]
      redirect_to show_page_path(@page)
    else
      flash[:error] = "Please fill all valid information."
      redirect_to show_page_path(@page)
    end
  end

  def destroy
    @page=Page.find(params[:id])
    puts " cloud_storage_type-->  ",@page.cloud_storage_type

    if @page.cloud_storage_type == "dropbox"
      delete_dropbox_folder(@page)
    elsif @page.cloud_storage_type == "googledrive"
      delete_google_drive_folder(@page)
    elsif @page.cloud_storage_type == "onedrive"
      delete_one_drive_folder(@page)
    else
      redirect_to pages_path
    end
  end

  def delete_dropbox_folder(page)
    begin
      if page.present?
        if page.dropbox_infos.present?
          dbx = Dropbox::Client.new(page.user.dropbox_detail.access_token)
          dbx.delete("/Apps/CloudTransfr/#{page.name}")
        end
        page.destroy
        flash[:notice] = "Page deleted successfully."
        redirect_to pages_path
      else
        flash[:error] = "Page is not exist."
        redirect_to pages_path
      end
    rescue Exception => e
      puts "Error: #{e.message}"
      flash[:error] = "Error: #{e.message}"
      redirect_to pages_path
    end
  end

  def delete_google_drive_folder(page)
    begin
      if page.present?
        if !page.user.google_detail.refresh_token.present?
          puts "access_token expire --->"
          flash[:error] = "Your one drive access token is expire. So, please authenticate it again."
          redirect_to staticpage_index_path
        else
          @credentials = google_drive_credential
          @credentials.refresh_token = page.user.google_detail.refresh_token
          @credentials.fetch_access_token!
          puts "@credentials ---> ",@credentials.inspect

          # DELETE https://www.googleapis.com/drive/v2/files/FILE_ID
          uri = URI("https://www.googleapis.com/drive/v2/files/#{page.folder_id}")

          Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
            request = Net::HTTP::Delete.new uri
            request["authorization"] = "Bearer #{@credentials.access_token}"
            response = http.request request # Net::HTTPResponse object
            puts "response --> ",response.inspect
            flash[:notice] = "Page deleted successfully."
            page.destroy
          end
          redirect_to pages_path
        end
      else
        flash[:error] = "Page is not exist."
        redirect_to pages_path
      end
    rescue Exception => e
      flash[:error] = "Error: #{e.message}"
      redirect_to pages_path
    end
  end

  def delete_one_drive_folder(page)
    begin
      if page.present?
        if !page.user.onedrive_detail.refresh_token.present?
          puts "access_token expire --->"
          flash[:error] = "Your one drive access token is expire. So, please authenticate it again."
          redirect_to staticpage_index_path
        else
    			get_new_onedrive_access_token(page)
          @access_token = page.user.onedrive_detail.access_token
          # DELETE https://apis.live.net/v5.0/folder.a6b2a7e8f2515e5e.A6B2A7E8F2515E5E!114?access_token=ACCESS_TOKEN
          uri = URI("https://apis.live.net/v5.0/#{page.folder_id}?access_token=#{@access_token}")

          Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
            request = Net::HTTP::Delete.new uri
            response = http.request request # Net::HTTPResponse object
            puts "response --> ",response.inspect
            flash[:notice] = "Page deleted successfully."
            page.destroy
          end
          redirect_to pages_path
        end
      else
        flash[:error] = "Page is not exist."
        redirect_to pages_path
      end
    rescue Exception => e
      flash[:error] = "Error: #{e.message}"
      redirect_to pages_path
    end
  end

  def show
    @user = current_user
  	@page = Page.friendly.find(params[:id])
  	created_before =  ((Time.now()-@page.updated_at)/ 1.hour).round
  	@expire =  created_before > 48 ? true : false
    @access_token = @page.user.dropbox_detail.access_token
    client = DropboxClient.new(@access_token)
    # client.metadata('/', file_limit=25000, list=true, hash=nil, rev=nil, include_deleted=false)
    @files_total = client.metadata('/', file_limit=25000, list=true, hash=nil, rev=nil, include_deleted=false)
    paths =  @files_total["contents"].map{|hash| hash["path"]}
    @paths = @page.dropbox_infos.where(path: paths).distinct.pluck(:path)
  end

  def show_page
    @page = Page.friendly.find(params[:id])
    @user = @page.user
    if @page.cloud_storage_type == "dropbox"
      @paths = @page.dropbox_infos.distinct
    elsif @page.cloud_storage_type == "googledrive"
      @paths = @page.google_infos.distinct
    elsif @page.cloud_storage_type == "onedrive"
      @paths = @page.onedrive_infos.distinct
    end
  end

  def upload
    @page = Page.friendly.find(params[:id])
    current_size = @page.user.user_uploaded_file_size_total.total_size
		limit = params[:limit].to_i.round(2)
		kilobytes = (params[:filesize].to_i / 1024)
		megabytes = (kilobytes / 1024)
		file_of_size = megabytes.round(2)
		file_of_size = file_of_size + current_size
    if file_of_size > limit
      render json: { error: "You don't have enough space to upload this file"}, :status => 400
    else
      @page = Page.friendly.find(params[:id])
      if @page.cloud_storage_type == "googledrive"
        google_drive_upload(@page)
      elsif @page.cloud_storage_type == "dropbox"
        dropbox_upload(@page)
      elsif @page.cloud_storage_type == "onedrive"
        onedrive_upload(@page)
      end
    end
  end

  def google_drive_upload(page)
    begin
      @page = page
      @credentials = google_drive_credential
      @credentials.refresh_token = @page.user.google_detail.refresh_token
      @credentials.fetch_access_token!
      session = GoogleDrive::Session.from_credentials(@credentials)
      folder1 = session.collection_by_title(@page.name)

      @file_size = ((params[:file].size.to_f / 1024) / 1024 ).round(2)
      # If folder is not exist on google drive then create folder on google dirve
      if folder1.blank?
        @new_folder = session.root_collection.create_subcollection(@page.name)
        @page.update(folder_id: @new_folder.id)
      end

      @page.user.google_detail.update(access_token: @credentials.access_token)

      # Initialize the API
      service = Google::Apis::DriveV3::DriveService.new
      service.client_options.application_name = "gdrive project"
      service.authorization = @page.user.google_detail.access_token
      file = open(params[:file])
      file_name = original_filename = params[:file].original_filename
      original_filename = params[:file].original_filename
      content_type = params[:file].content_type

      # Check existing file name
      @get_google_infos = @page.google_infos.where(original_filename: original_filename)
      if @get_google_infos.present?
        f_name = original_filename.rpartition('.').first
        file_extension = original_filename.rpartition('.').last
        uploaded_filename = "#{f_name} (#{@get_google_infos.count}).#{file_extension}"
      else
        uploaded_filename = original_filename
      end

      folder_id = @page.folder_id
      file_metadata = { name: uploaded_filename, parents: [folder_id] }
      file = service.create_file(file_metadata, fields: 'id', upload_source: file, content_type: content_type)

      if user_signed_in? && current_user.id == @page.user.id
        google_info = @page.google_infos << GoogleInfo.new(:original_filename => original_filename, :uploaded_filename => uploaded_filename, :file_id => file.id,:folder_id => @page.folder_id,:modified => Time.now,:mine_type => content_type,:path => file_name,:size => @file_size,:client_mtime => Time.now,:uploaded_by => "Owner")
      else
        google_info = @page.google_infos << GoogleInfo.new(:original_filename => original_filename, :uploaded_filename => uploaded_filename, :file_id => file.id,:folder_id => @page.folder_id,:modified => Time.now,:mine_type => content_type,:path => file_name,:size => @file_size,:client_mtime => Time.now,:uploaded_by => "Other")
      end

      if @page.email_frequency
        @download_url = googledrive_download_path(@page.name, path: @page.google_infos.last.id)
        if @page.page_cc_email.present?
          CloudUploadMailer.upload_file(@page.user,@page,@page.google_infos.last,@download_url,@page.page_cc_email.cc_email).deliver
        else
          CloudUploadMailer.upload_file(@page.user,@page,@page.google_infos.last,@download_url).deliver
        end
      end
      redirect_to pages_path, notice: 'Successfully uploaded the file.'
    rescue Exception => e
      puts "Error --> ",e.message
      redirect_to pages_path, error: "Error: #{e.message}"
    end
  end

  def dropbox_upload(page)
    @page = page
    begin
      @access_token = @page.user.dropbox_detail.access_token
      client = DropboxClient.new(@access_token)
      puts "linked account:", client.account_info().inspect
      file = open(params[:file])
      original_filename = params[:file].original_filename
      @get_dropbox_infos = @page.dropbox_infos.where(original_filename: original_filename)
      @file_size = ((params[:file].size.to_f / 1024) / 1024 ).round(2)
      if @get_dropbox_infos.present?
        f_name = original_filename.rpartition('.').first
        file_extension = original_filename.rpartition('.').last
        uploaded_filename = "#{f_name} (#{@get_dropbox_infos.count}).#{file_extension}"
      else
        uploaded_filename = original_filename
      end
      file_name = params[:file].original_filename
      f_name = "/Apps/CloudTransfr/#{@page.name}/#{uploaded_filename}"
      response = client.put_file(f_name, file)
      puts "uploaded:", response
      if user_signed_in? && current_user.id == @page.user.id
        dropbox_info = @page.dropbox_infos << DropboxInfo.new(:revision => response["revision"],:rev => response["rev"],:original_filename => original_filename, :uploaded_filename => uploaded_filename,:modified => response["modified"],:mime_type => response["mime_type"],:path => response["path"],:size => @file_size,:client_mtime => response["client_mtime"],:uploaded_by => "Owner")
      else
        dropbox_info = @page.dropbox_infos << DropboxInfo.new(:revision => response["revision"],:rev => response["rev"],:original_filename => original_filename, :uploaded_filename => uploaded_filename,:modified => response["modified"],:mime_type => response["mime_type"],:path => response["path"],:size => @file_size,:client_mtime => response["client_mtime"],:uploaded_by => "Other")
      end
      if @page.email_frequency
        @download_url = dropbox_download_path(@page.name, path: @page.dropbox_infos.last.id)
        if @page.page_cc_email.present?
          CloudUploadMailer.upload_file(@page.user,@page,@page.dropbox_infos.last,@download_url,@page.page_cc_email.cc_email).deliver
        else
          CloudUploadMailer.upload_file(@page.user,@page,@page.dropbox_infos.last,@download_url).deliver
        end
      end
      redirect_to pages_path, notice: 'Successfully uploaded the file.'
    rescue Exception => e
      puts "Error --> ",e.message
      redirect_to pages_path, error: "Error: #{e.message}"
    end
  end

  def onedrive_upload(page)
    @page = page
    begin
      if !@page.user.onedrive_detail.refresh_token.present?
        flash[:error] = "Your one drive access token is expire. So, please authenticate it again."
        redirect_to staticpage_index_path
      else
        get_new_onedrive_access_token(@page)
        @access_token = @page.user.onedrive_detail.access_token

        if @page.folder_id == nil && @page.cloud_storage_type == "onedrive"
          @onedrive_folder = JSON.parse(create_onedrive_folder(@page))
          @page.update(cloud_storage_id: @onedrive_folder.id)
        end

        if @page.folder_id != nil
          @onedrive_upload_response = upload_file_to_onedrive(params)
          if @onedrive_upload_response.present?
            @response_file = JSON.parse(@onedrive_upload_response[:response].read_body)

            if @response_file["error"].present? && @response_file["error"]["code"] == "resource_not_found"
              @onedrive_folder = JSON.parse(create_onedrive_folder(@page))
              @page.update(folder_id: @onedrive_folder["id"])

              @onedrive_upload_response = upload_file_to_onedrive(params)
              @response_file = JSON.parse(@onedrive_upload_response[:response].read_body)
            end
            @file_size = ((params[:file].size.to_f / 1024) / 1024 ).round(2)
            if @response_file["id"].present?
              if user_signed_in? && current_user.id == @page.user.id
                onedrive_info = @page.onedrive_infos << OnedriveInfo.new(:original_filename => @onedrive_upload_response[:original_filename],:uploaded_filename => @onedrive_upload_response[:uploaded_filename], :file_id => @response_file["id"],:source => @response_file["source"],:modified => Time.now,:mine_type => content_type,:path => @response_file["name"],:size => @file_size,:client_mtime => Time.now,:uploaded_by => "Owner")
              else
                onedrive_info = @page.onedrive_infos << OnedriveInfo.new(:original_filename => @onedrive_upload_response[:original_filename],:uploaded_filename => @onedrive_upload_response[:uploaded_filename], :file_id => @response_file["id"],:source => @response_file["source"],:modified => Time.now,:mine_type => content_type,:path => @response_file["name"],:size => @file_size,:client_mtime => Time.now,:uploaded_by => "Other")
              end
              if @page.email_frequency
                @download_url = onedrive_download_path(@page.name, path: @page.onedrive_infos.last.id)
                if @page.page_cc_email.present?
                  CloudUploadMailer.upload_file(@page.user,@page,@page.onedrive_infos.last,@download_url,@page.page_cc_email.cc_email).deliver
                else
                  CloudUploadMailer.upload_file(@page.user,@page,@page.onedrive_infos.last,@download_url).deliver
                end
              end
              redirect_to show_page_path(@page), notice: 'Successfully uploaded the file.'
              puts "not have an error --> "
            elsif @response_file["error"].present? && @response_file["error"]["code"] == "resource_already_exists"
              flash[:error] = @response_file["error"]["message"]
              redirect_to show_page_path(@page)
            end
          end
        end
      end
    rescue Exception => e
      puts "Eroor --> ",e
      redirect_to pages_path, error: "Error: #{e.message}"
    end
  end

  def upload_file_to_onedrive(params)
    puts "upload_file_to_onedrive params --> ",params.inspect
    @url = "https://apis.live.net/v5.0/#{@page.folder_id}/files?overwrite=false&access_token=#{@access_token}"
    uri = URI.parse(@url)

    boundary = (0...6).map { (65 + rand(26)).chr }.join
    file = open(params[:file])
    cloudFileName = file_name = params[:file].original_filename
    original_filename = params[:file].original_filename

    # @get_onedrive_infos = @page.onedrive_infos.where("original_filename = (?) OR uploaded_filename = (?)", original_filename, original_filename)
    @get_onedrive_infos = @page.onedrive_infos.where(original_filename: original_filename) + @page.onedrive_infos.where(uploaded_filename: original_filename)
    puts "---- get_onedrive_infos --- ",@get_onedrive_infos.inspect
    if @get_onedrive_infos.present?
      puts "---- ****** get_onedrive_infos present ****** --- "
      f_name = original_filename.rpartition('.').first
      file_extension = original_filename.rpartition('.').last
      uploaded_filename = "#{f_name} (#{@get_onedrive_infos.count}).#{file_extension}"
    else
      uploaded_filename = original_filename
    end
    fileMIMEType = params[:file].content_type

    post_body = []
    post_body << "--#{boundary}\r\n"
    post_body << "Content-Disposition: form-data; name=\"file\"; filename=\"#{uploaded_filename}\"\r\n"
    post_body << "Content-Type: #{fileMIMEType}\r\n\r\n"
    post_body << File.read(file)
    post_body << "\r\n--#{boundary}--\r\n"

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "multipart/form-data; boundary=#{boundary}"
    request.body = post_body.join
    response = http.request(request)
    return {response: response,original_filename: original_filename, uploaded_filename: uploaded_filename}
  end

  def create_onedrive_folder(page)
    get_new_onedrive_access_token(page)
    @access_token = page.user.onedrive_detail.access_token
    # https://apis.live.net/v5.0/me/skydrive
    url = URI("https://apis.live.net/v5.0/me/skydrive")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["authorization"] = "Bearer #{@access_token}"
    request["content-type"] = 'application/json'
    request.body = "{\n    \"name\": \"#{page.name}\"\n}"  # {name: page.name}

    response = http.request(request)
    puts "response----> ",response.read_body
    response.read_body
  end

  def dropbox_download
    @d_info = DropboxInfo.find(params[:path])
    client = DropboxClient.new(@d_info.page.user.dropbox_detail.access_token)
    puts "linked account:", client.account_info().inspect
    contents, metadata = client.get_file_and_metadata(@d_info.path)
    if @d_info.uploaded_filename.present?
      file_new = @d_info.uploaded_filename
    else
      file_new = @d_info.path.tr('/', '')
    end
    send_data( contents, :filename => file_new )
  end

  def remove_from_dropbox
    begin
      @previous_url = request.referrer
      @d_info = DropboxInfo.find(params[:path])
      if @d_info.present?
        dbx = Dropbox::Client.new(@d_info.page.user.dropbox_detail.access_token)
        dbx.delete(@d_info.path)
        @d_info.delete
        # minus size after file delete
        if current_user.user_uploaded_file_size_total.present? && current_user.user_uploaded_file_size_total.total_size.present?
          count_size = current_user.user_uploaded_file_size_total.total_size.round(2)
          total_size = count_size - @one_info.size.to_f
          current_user.user_uploaded_file_size_total.update(total_size: total_size)
        end
        flash[:notice] = "File deleted successfully."
        redirect_back(fallback_location: @previous_url)
      else
        flash[:error] = "File was not deleted."
        redirect_back(fallback_location: @previous_url)
      end
    rescue Exception => e
      puts "Error----> ",e.message
      flash[:error] = "Error: #{e.message}"
      redirect_back(fallback_location: @previous_url)
    end
  end

  def googledrive_download
    @g_info = GoogleInfo.find(params[:path])
    @previous_url = request.referrer
    @credentials = google_drive_credential
    puts "access_token valid --->"
    @credentials.refresh_token = @g_info.page.user.google_detail.refresh_token
    @credentials.fetch_access_token!
    puts "credentials --->  ",@credentials.inspect
    @g_info.page.user.google_detail.update(access_token: @credentials.access_token)

    url = URI("https://www.googleapis.com/drive/v2/files/#{@g_info.file_id}")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(url)
    request["authorization"] = "Bearer #{@credentials.access_token}"

    response = http.request(request)
    puts "response----> ",response.read_body

    @google_response = JSON.parse(response.read_body)
    puts "@google_response --> ",@google_response.inspect

    if @google_response["error"].present?
      flash[:error] = @google_response['error']['message']
      redirect_back(fallback_location: @previous_url)
    else
      puts "@google_response webContentLink --> ",@google_response["webContentLink"]
      redirect_to @google_response["webContentLink"]
    end
  end

  def remove_from_googledrive
    @google_info = GoogleInfo.find(params[:path])
    @previous_url = request.referrer
    if !@google_info.page.user.google_detail.refresh_token.present?
      puts "access_token expire --->"
      flash[:error] = "Your one drive access token was expired. So, please authenticate it again."
      redirect_back(fallback_location: @previous_url)
    else
      @credentials = google_drive_credential
      @credentials.refresh_token = @google_info.page.user.google_detail.refresh_token
      @credentials.fetch_access_token!
      puts "@credentials ---> ",@credentials.inspect

      # DELETE https://www.googleapis.com/drive/v2/files/FILE_ID
      uri = URI("https://www.googleapis.com/drive/v2/files/#{@google_info.file_id}")

      Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
        request = Net::HTTP::Delete.new uri
        request["authorization"] = "Bearer #{@credentials.access_token}"
        response = http.request request # Net::HTTPResponse object
        puts "response --> ",response.inspect
        flash[:notice] = "File deleted successfully."
        @google_info.delete
        # minus size after file delete
        if current_user.user_uploaded_file_size_total.present? && current_user.user_uploaded_file_size_total.total_size.present?
          count_size = current_user.user_uploaded_file_size_total.total_size.round(2)
          total_size = count_size - @google_info.size.to_f
          current_user.user_uploaded_file_size_total.update(total_size: total_size)
        end
      end
      redirect_back(fallback_location: @previous_url)
    end
  end

  def onedrive_download
    @one_info = OnedriveInfo.find(params[:path])
    @previous_url = request.referrer
    if !@one_info.page.user.onedrive_detail.refresh_token.present?
      puts "access_token expire --->"
      flash[:error] = "User one drive access token was expired. So, please authenticate it again."
      redirect_back(fallback_location: @previous_url)
    else
      get_new_onedrive_access_token(@one_info.page)
      @access_token = @one_info.page.user.onedrive_detail.access_token
      # Get a download link to the contents of a file, photo, video, or audio
      # https://apis.live.net/v5.0/file.[fileid]/content?suppress_redirects=true?access_token=ACCESS_TOKEN
      @uri = "https://apis.live.net/v5.0/#{@one_info.file_id}/content?download=true&access_token=#{@access_token}"

      url = URI("https://apis.live.net/v5.0/#{@one_info.file_id}/content?download=true&access_token=#{@access_token}")

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(url)
      response = http.request(request)
      puts "response----> ",response
      if response.read_body.present?
        puts "response----> ",response.read_body
        @onedrive_response = JSON.parse(response.read_body)
        puts "@onedrive_response --> ",@onedrive_response.inspect
        if @onedrive_response["error"].present? && @onedrive_response["error"]["code"] == "resource_not_found"
          flash[:error] = @onedrive_response['error']['message']
          redirect_back(fallback_location: @previous_url)
        else
          flash[:error] = @onedrive_response['error']['message']
          redirect_back(fallback_location: @previous_url)
        end
      else
        redirect_to @uri
      end

    end
  end

  def remove_from_onedrive
    @one_info = OnedriveInfo.find(params[:path])
    @previous_url = request.referrer
    begin
      if @one_info.present?
        if !@one_info.page.user.onedrive_detail.refresh_token.present?
          puts "access_token expire --->"
          flash[:error] = "Your one drive access token is expire. So, please authenticate it again."
          redirect_back(fallback_location: @previous_url)
        else
    			get_new_onedrive_access_token(@one_info.page)
          @access_token = @one_info.page.user.onedrive_detail.access_token
          # DELETE https://apis.live.net/v5.0/file.b7c3b8f9g3616f6f.B7CB8F9G3626F6!225?access_token=ACCESS_TOKEN
          uri = URI("https://apis.live.net/v5.0/#{@one_info.file_id}?access_token=#{@access_token}")

          Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
            request = Net::HTTP::Delete.new uri
            response = http.request request # Net::HTTPResponse object
            puts "response --> ",response.inspect
            flash[:notice] = "File deleted successfully."
            @one_info.delete
            # minus size after file delete
            if current_user.user_uploaded_file_size_total.present? && current_user.user_uploaded_file_size_total.total_size.present?
              count_size = current_user.user_uploaded_file_size_total.total_size.round(2)
              total_size = count_size - @one_info.size.to_f
              current_user.user_uploaded_file_size_total.update(total_size: total_size)
            end
          end
          redirect_back(fallback_location: @previous_url)
        end
      else
        flash[:error] = "File is not exist."
        redirect_back(fallback_location: @previous_url)
      end
    rescue Exception => e
      flash[:error] = "Error: #{e.message}"
      redirect_back(fallback_location: @previous_url)
    end
  end

  private

	def page_params
		params.require(:page).permit(:name,:user_id,:is_active,:cloud_storage_id,:cloud_storage_type,:email_frequency,:white_label_title )
	end

end
