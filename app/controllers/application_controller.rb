class ApplicationController < ActionController::Base
  include ApplicationHelper
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def check_payment
    if user_signed_in? && current_user.created_at < Time.now - 7.days
      if !current_user.user_subscriptions.present?
        redirect_to switch_plan_path
      else
        return true
      end
    else
      return true
    end
  end

  def authenticate_any!
    if admin_signed_in?
      true
    else
      authenticate_user!
    end
  end

  def google_drive_credential
    if Rails.env == "development"
      @credentials = Google::Auth::UserRefreshCredentials.new(
      client_id: ENV["GOOGLE_DRIVE_CLIENT_ID"],
      client_secret: ENV["GOOGLE_DRIVE_CLIENT_SECRET"],
      scope: [
        "https://www.googleapis.com/auth/drive",
        "https://spreadsheets.google.com/feeds/",
        "https://www.googleapis.com/auth/drive.appfolder",
        "https://www.googleapis.com/auth/drive.file"
      ],
      access_type: 'offline',
      redirect_uri: ENV["GOOGLE_DRIVE_DEV_REDIRECT_URL"])
    else
      @credentials = Google::Auth::UserRefreshCredentials.new(
      client_id: ENV["GOOGLE_DRIVE_CLIENT_ID"],
      client_secret: ENV["GOOGLE_DRIVE_CLIENT_SECRET"],
      scope: [
        "https://www.googleapis.com/auth/drive",
        "https://spreadsheets.google.com/feeds/",
        "https://www.googleapis.com/auth/drive.appfolder",
        "https://www.googleapis.com/auth/drive.file"
      ],
      access_type: 'offline',
      redirect_uri: ENV["GOOGLE_DRIVE_PROD_REDIRECT_URL"])
    end
    return @credentials
  end

  # get onedrive access_token from refresh_token
	def get_new_onedrive_access_token(page)
    @page = page
    if Rails.env == "development"
      onedrive_url = ENV["ONE_DRIVE_DEV_REDIRECT_URL"]
    else
      onedrive_url = ENV["ONE_DRIVE_PROD_REDIRECT_URL"]
    end
  	onedrive_params = {refresh_token: @page.user.onedrive_detail.refresh_token,client_id: ENV["ONE_DRIVE_CLIENT_ID"],client_secret: ENV["ONE_DRIVE_CLIENT_SECRET"],redirect_uri: onedrive_url,grant_type: 'refresh_token' }
  	onedrive_response = Net::HTTP.post_form(URI.parse('https://login.live.com/oauth20_token.srf'), onedrive_params)
    puts "onedrive_response.body ---> ",onedrive_response.body
    @onedrive_response = JSON.parse(onedrive_response.body)
    @page.user.onedrive_detail.update_attributes(:access_token => @onedrive_response["access_token"],:token_type => @onedrive_response["token_type"], :refresh_token => @onedrive_response["refresh_token"], :expires_in => @onedrive_response["expires_in"], :expires_at => Time.now + @onedrive_response["expires_in"].to_i ,:uid => @onedrive_response["user_id"],:account_id => params[:code],:is_active => true)
  end

  protected
    def configure_permitted_parameters
       devise_parameter_sanitizer.permit(:sign_up, keys: [:pagename])
    end
end
