class SubscriptionsController < ApplicationController
  require "stripe"
  def idnex
  end

  def create
    subscription = Stripe::Plan.create(
      :amount => (params[:amount].to_i)*100,
      :interval => params[:interval],
      :name => params[:name],
      :currency => 'usd',
      :trial_plan => nil,
      :id => SecureRandom.uuid # This ensures that the plan is unique in stripe
    )
    puts "subscription --> #{subscription.inspect}"

    if subscription
      SubscriptionPlan.create(name: params[:name],plan_id: subscription[:id] ,interval:  params[:interval],amount: params[:amount].to_i )
      #Save the response to your DB
      flash[:notice] = "Plan successfully created"
      redirect_to '/subscription'
    else
      flash[:error] = "Plan successfully was not created"
      render 'index'
    end
  end
end
