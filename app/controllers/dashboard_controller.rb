class DashboardController < ApplicationController
  include DashboardHelper
  before_action :authenticate_user!, except: [:privacy_policy, :index]
  after_action :update_user_uploaded_file_size_total, only: [:subscription_checkout]
  before_action :downgrade_user_plan, only: [:subscription_checkout]
  def index
    @pages=Page.new
    @plans = SubscriptionPlan.all
    if user_signed_in? && current_user.user_subscriptions.present?
      @current_plan = current_user.user_subscriptions.last.subscription_plan.plan_id
      @decline_request = current_user.payment_decline_requests
    end
  end

  def privacy_policy
  end

  def switch_plan
    if user_signed_in? && current_user.user_subscriptions.present?
      @current_plan = current_user.user_subscriptions.last.subscription_plan.plan_id
    end
    @decline_request = current_user.payment_decline_requests
    @plans = SubscriptionPlan.all
  end

  def addcard
    # Stripe secret key
    Stripe.api_key = ENV["STRIPE_SECRET_KEY"]
    if user_signed_in? && current_user.user_subscriptions.last.present?
      # Retrieve stripe existing customer
      customer = Stripe::Customer.retrieve(current_user.user_subscriptions.last.stripe_customer_id)
      @card = customer.sources.retrieve(customer[:default_source])
    end
  end

  def payment_decline_request
    @previous_url = request.referrer
    @decline_request = PaymentDeclineRequest.new(decline_params)
    if @decline_request.save
      flash[:notice] = "Payment decline request was sent to admin."
      redirect_back(fallback_location: @previous_url)
    else
      flash[:error] = "Payment decline request was not sent to admin."
      redirect_back(fallback_location: @previous_url)
    end
  end

  def subscription_checkout
     plan_id = params[:plan_id]
     plan = Stripe::Plan.retrieve(plan_id)

     # Fetch subscription plan
     @s_plan = SubscriptionPlan.find_by_plan_id(plan.id)

     @last_subscriptions = current_user.user_subscriptions.last
    if user_signed_in? && @last_subscriptions.present?
      if @last_subscriptions.subscription_plan.plan_id === plan.id && @last_subscriptions.subscription_end_priod > Time.now
        flash[:notice] = "Your #{@last_subscriptions.subscription_plan.name} subscription plan is already active. It will expire on #{@last_subscriptions.subscription_end_priod}."
        redirect_to switch_plan_path
      elsif @last_subscriptions.subscription_plan.plan_id != plan.id

        # Update existing stripe customer details
        customer = Stripe::Customer.retrieve(@last_subscriptions.stripe_customer_id)
        puts "customer --> #{customer.inspect}"

        # Retrieve customer subscriptions
        stripe_subscription = customer.subscriptions.retrieve(@last_subscriptions.stripe_subscription_id)

        # stripe_subscription = Stripe::Subscription.retrieve(@last_subscriptions.stripe_subscription_id)
        puts "--------------retrieve stripe_subscription --> #{stripe_subscription.inspect}"
        stripe_subscription.plan = plan.id
        stripe_subscription.save
        puts "Updated plan New stripe_subscription --> #{stripe_subscription.inspect}"

        stripe_invoice = Stripe::Invoice.create(:customer => stripe_subscription[:customer])
        puts "stripe_invoice --> #{stripe_invoice.inspect}"

        retrieve_stripe_invoice = Stripe::Invoice.retrieve(stripe_invoice.id)
        retrieve_stripe_invoice.pay

        charge = Stripe::Charge.list(customer: stripe_subscription[:customer])
        puts "charge all --> #{charge.inspect}"
        puts "last charge --> #{charge.data.first}"

        if (plan.amount/100) < @last_subscriptions.subscription_plan.amount && stripe_invoice.total < 0
          refund = Stripe::Refund.create(
            :charge => @last_subscriptions.stripe_charge_id,
            :amount => stripe_invoice.total.abs,
          )
          puts "refund --> #{refund.inspect}"
        end
      else
        # Update existing stripe customer details
        customer = Stripe::Customer.retrieve(@last_subscriptions.stripe_customer_id)

        # Create new subscription
        stripe_subscription = customer.subscriptions.create(:plan => plan.id)

        charge = Stripe::Charge.list(customer: stripe_subscription[:customer])
        # puts "charge all --> #{charge.inspect}"
        # puts "last charge --> #{charge.data.first}"
      end
    else
      # Create new stripe customer
      customer = Stripe::Customer.create(
        :description => "Customer for #{current_user.email}",
        :source => params[:stripeToken],
        :email => current_user.email
      )
      stripe_subscription = customer.subscriptions.create(:plan => plan.id)

      charge = Stripe::Charge.list(customer: stripe_subscription[:customer])
      # puts "charge all --> #{charge.inspect}"
      # puts "last charge --> #{charge.data.first}"
    end

    if stripe_subscription.present? && charge.data.first.present?
      # Save this in your DB and associate with the user;s email
      current_user.user_subscriptions.create(subscription_plan_id: @s_plan.id,stripe_subscription_id: stripe_subscription[:id], stripe_customer_id: stripe_subscription[:customer], stripe_payment_status: stripe_subscription[:status], subscription_start_priod: Time.at(stripe_subscription[:current_period_start].to_i), subscription_end_priod: Time.at(stripe_subscription[:current_period_end].to_i), livemode: stripe_subscription[:livemode], stripe_charge_id: charge.data.first.id,stripe_invoice_id: charge.data.first.invoice)

      flash[:notice] = "Successfully active a subscription plan."
      redirect_to switch_plan_path
    end
  end

  private
  def decline_params
		params.require(:decline).permit(:user_id,:subscription_plan_id,:comment)
	end
end
