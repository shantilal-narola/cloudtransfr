class StaticpageController < ApplicationController
	include StaticpageHelper
	require 'net/http'
	require 'net/https'
	require "googleauth"
	require "openssl"
  require 'open-uri'
	# helper ApplicationHelper

	OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
	before_action :authenticate_user!, except: [:index]
	before_action :check_free_user, only: [:index]
	before_action :check_plan, only: [:index]
	before_action :check_pages, only: [:index]

	def index
		if user_signed_in? && current_user.pages.last.cloud_storage_type.blank?
			 if current_user.dropbox_detail.present? && current_user.pages.count <= 1
				 current_user.pages.last.update(cloud_storage_type: "dropbox")
			 elsif current_user.google_detail.present? && current_user.pages.count <= 1
				 @credentials = google_drive_credential
				 @credentials.refresh_token = current_user.google_detail.refresh_token
				 @credentials.fetch_access_token!
				 @a_token =  @credentials.access_token
				 current_user.google_detail.update(access_token: @credentials.access_token)
				 session = GoogleDrive::Session.from_credentials(@credentials)

				 @new_folder = session.root_collection.create_subcollection(current_user.pages.last.name)
				 current_user.pages.last.update(cloud_storage_type: "googledrive",folder_id: @new_folder.id)
				 puts "@new_folder ---> ",@new_folder.inspect

			 elsif current_user.onedrive_detail.present? && current_user.pages.count <= 1
				 get_new_onedrive_access_token(current_user.pages.last)
		     @access_token = current_user.onedrive_detail.access_token
		     # https://apis.live.net/v5.0/me/skydrive
		     url = URI("https://apis.live.net/v5.0/me/skydrive")

		     http = Net::HTTP.new(url.host, url.port)
		     http.use_ssl = true
		     http.verify_mode = OpenSSL::SSL::VERIFY_NONE

		     request = Net::HTTP::Post.new(url)
		     request["authorization"] = "Bearer #{@access_token}"
		     request["content-type"] = 'application/json'
		     request.body = "{\n    \"name\": \"#{current_user.pages.last.name}\"\n}"
		     response = http.request(request)
				 @onedrive_folder = JSON.parse(response.read_body)
				 current_user.pages.last.update(cloud_storage_type: "onedrive",folder_id: @onedrive_folder["id"])
			 end
		end

	  # for google drive
		@credentials = google_drive_credential

  	if Rails.env == "development"
			# for dropbox
  		@dropbox_url = "https://www.dropbox.com/oauth2/authorize?client_id=#{ENV["DROPBOX_CLIENT_ID"]}&response_type=code&redirect_uri=#{ENV["DROPBOX_DEV_REDIRECT_URL"]}"

			# for onedrive
			@onedrive_url = "https://login.live.com/oauth20_authorize.srf?client_id=#{ENV["ONE_DRIVE_CLIENT_ID"]}&scope=offline_access onedrive.appfolder onedrive.readwrite wl.skydrive wl.skydrive_update&response_type=code&redirect_uri=#{ENV["ONE_DRIVE_DEV_REDIRECT_URL"]}"
			# @onedrive_url = "https://login.live.com/oauth20_authorize.srf?client_id=#{ENV["ONE_DRIVE_CLIENT_ID"]}&scope=User.Read&response_type=token&redirect_uri=#{ENV["ONE_DRIVE_DEV_REDIRECT_URL"]}"
  	else
			# for dropbox
  		@dropbox_url = "https://www.dropbox.com/oauth2/authorize?client_id=#{ENV["DROPBOX_CLIENT_ID"]}&response_type=code&redirect_uri=#{ENV["DROPBOX_PROD_REDIRECT_URL"]}"

			# for onedrive
			@onedrive_url = "https://login.live.com/oauth20_authorize.srf?client_id=#{ENV["ONE_DRIVE_CLIENT_ID"]}&scope=offline_access onedrive.appfolder onedrive.readwrite wl.skydrive wl.skydrive_update&response_type=code&redirect_uri=#{ENV["ONE_DRIVE_PROD_REDIRECT_URL"]}"
		end

		@googledrive_url = @credentials.authorization_uri.to_s
		@googledrive_url = @googledrive_url + "&approval_prompt=force"
		puts "@googledrive_url --> ",@googledrive_url
  end

	def dropbox
		if Rails.env == "development"
  		@redirect_uri = ENV["DROPBOX_DEV_REDIRECT_URL"]
  	else
  		@redirect_uri = ENV["DROPBOX_PROD_REDIRECT_URL"]
  	end
		if params[:code].present?
			@dropbox_response = JSON.parse(get_from_dropbox(params[:code],@redirect_uri))
			if current_user.dropbox_detail.present?
				current_user.dropbox_detail.update_attributes(:access_token => @dropbox_response["access_token"],:token_type => @dropbox_response["token_type"],:uid => @dropbox_response["uid"],:account_id => @dropbox_response["account_id"],:is_active => true)
			else
				dropbox_details = current_user.create_dropbox_detail(:access_token => @dropbox_response["access_token"],:token_type => @dropbox_response["token_type"],:uid => @dropbox_response["uid"],:account_id => @dropbox_response["account_id"],:is_active => true)
				dropbox_details.save
			end
		end
		redirect_to staticpage_index_path
	end

	def googledrive
		if params[:error].present?
			redirect_to staticpage_index_path
		elsif params[:code].present?
			@credentials = google_drive_credential
			@credentials.code = params[:code]
			@credentials.fetch_access_token!
			puts "======================================="
			puts "@credentials ---> ",@credentials.inspect
			session = GoogleDrive::Session.from_credentials(@credentials)

			if @credentials.present?
				if current_user.google_detail.present?
					if @credentials.refresh_token.present?
						current_user.google_detail.update_attributes(:access_token => @credentials.access_token,:token_type => "Bearer",:code => params[:code],:refresh_token => @credentials.refresh_token, :expires_at => Time.at(@credentials.expires_at), :expire => @credentials.expiry, :is_active => true)
					else
						current_user.google_detail.update_attributes(:access_token => @credentials.access_token,:token_type => "Bearer",:code => params[:code], :expires_at => Time.at(@credentials.expires_at), :expire => @credentials.expiry, :is_active => true)
					end
				else
					google_details = current_user.create_google_detail(:access_token => @credentials.access_token,:token_type => "Bearer",:code => params[:code],:refresh_token => @credentials.refresh_token, :expires_at => Time.at(@credentials.expires_at), :expire => @credentials.expiry, :is_active => true)
					google_details.save
				end
				redirect_to staticpage_index_path
			else
				render json: session
			end
		end
	end

	def onedrive
		if Rails.env == "development"
  		@redirect_uri = ENV["ONE_DRIVE_DEV_REDIRECT_URL"]
  	else
  		@redirect_uri = ENV["ONE_DRIVE_PROD_REDIRECT_URL"]
  	end
		if params[:code].present?
			@onedrive_response = JSON.parse(get_from_onedrive(params[:code],@redirect_uri))
			puts " onedrive_response --> #{@onedrive_response.to_json}"
			if current_user.onedrive_detail.present?
				current_user.onedrive_detail.update_attributes(:access_token => @onedrive_response["access_token"],:token_type => @onedrive_response["token_type"], :refresh_token => @onedrive_response["refresh_token"], :expires_in => @onedrive_response["expires_in"], :expires_at => Time.now + @onedrive_response["expires_in"].to_i ,:uid => @onedrive_response["user_id"],:account_id => params[:code],:is_active => true)
			else
				onedrive_details = current_user.create_onedrive_detail(:access_token => @onedrive_response["access_token"],:token_type => @onedrive_response["token_type"], :refresh_token => @onedrive_response["refresh_token"], :expires_in => @onedrive_response["expires_in"], :expires_at => Time.now + @onedrive_response["expires_in"].to_i ,:uid => @onedrive_response["user_id"],:account_id => params[:code],:is_active => true)
				onedrive_details.save
			end
		end
		redirect_to staticpage_index_path
	end

  private

	# get dropbox access_token
	def get_from_dropbox(code,dropbox_url)
  	dropbox_params = {code: code,client_id: ENV["DROPBOX_CLIENT_ID"],client_secret: ENV["DROPBOX_CLIENT_SECRET"],redirect_uri: dropbox_url,grant_type: 'authorization_code' }
  	dropbox_response = Net::HTTP.post_form(URI.parse('https://api.dropboxapi.com/oauth2/token'), dropbox_params)
		dropbox_response.body
  end

	# get onedrive access_token
	def get_from_onedrive(code,onedrive_url)
  	onedrive_params = {code: code,client_id: ENV["ONE_DRIVE_CLIENT_ID"],client_secret: ENV["ONE_DRIVE_CLIENT_SECRET"],redirect_uri: onedrive_url,grant_type: 'authorization_code' }
  	onedrive_response = Net::HTTP.post_form(URI.parse('https://login.live.com/oauth20_token.srf'), onedrive_params)
		onedrive_response.body
  end
end
