module StaticpageHelper

  def g_drive_url(current_user,googledrive_url,free_user,authenticate_account,count,allow_authenticate)
    google_link = link_to(googledrive_url, class: "violat-btn") do
      "#{current_user.google_detail.present? ? 'Authenticated' : 'Authenticate'}"
    end
    if current_user.google_detail.present?
      google_link
    elsif free_user && authenticate_account
      google_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 1 && allow_authenticate == true && count < 1
      google_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 2 && count < 2
      google_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 3
      google_link
    end
  end
  def drop_box_url(current_user,dropbox_url,free_user,authenticate_account,count,allow_authenticate)
    drop_box_link = link_to(dropbox_url, class: "violat-btn") do
      "#{current_user.dropbox_detail.present? ? 'Authenticated' : 'Authenticate'}"
    end
    if current_user.dropbox_detail.present?
      drop_box_link
    elsif free_user && authenticate_account
      drop_box_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 1 && allow_authenticate == true && count < 1
      drop_box_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 2 && count < 2
      drop_box_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 3
      drop_box_link
    end
  end

  def one_drive_url(current_user,onedrive_url,free_user,authenticate_account,count,allow_authenticate)
    one_drive_link = link_to(onedrive_url, class: "violat-btn") do
      "#{current_user.onedrive_detail.present? ? 'Authenticated' : 'Authenticate'}"
    end
    if current_user.onedrive_detail.present?
      one_drive_link
    elsif free_user && authenticate_account
      one_drive_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 1 && allow_authenticate == true && count < 1
      one_drive_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 2 && count < 2
      one_drive_link
    elsif current_user.user_subscriptions.present? && current_user.user_subscriptions.last.subscription_plan_id == 3
      one_drive_link
    end
  end

  def check_pages
    unless current_user.pages.present?
			flash[:notice] = "Please create page before continue"
			redirect_to root_path
		end
  end
end
