module ApplicationHelper
  def check_free_user
    if user_signed_in? && !current_user.user_subscriptions.present?
			if Date.today < User.last.created_at + 7.days
				@free_user = true
				unless current_user.dropbox_detail.present? ||  current_user.google_detail.present? ||  current_user.onedrive_detail.present?
					@authenticate_account = true
				end
        if current_user.pages.count < 1
          @allow_pages = true
        end
			end
		end
  end
  def check_plan
    if user_signed_in? && current_user.user_subscriptions.present?
      @count = 0
      if current_user.dropbox_detail.present?
        @count = @count + 1
      end
      if current_user.google_detail.present?
        @count = @count + 1
      end
      if current_user.onedrive_detail.present?
        @count = @count + 1
      end

      if current_user.user_subscriptions.last.subscription_plan_id == 1
        unless current_user.dropbox_detail.present? ||  current_user.google_detail.present? ||  current_user.onedrive_detail.present?
          @authenticate_account = true
        end
        if current_user.pages.count < 1
          @allow_pages = true
        end
        if !current_user.dropbox_detail.present? ||  !current_user.google_detail.present? ||  !current_user.onedrive_detail.present?
          @allow_authenticate = true
        end
      elsif current_user.user_subscriptions.last.subscription_plan_id == 2
        if current_user.pages.count < 10
          @plan_two_pages = true
        end
      elsif current_user.user_subscriptions.last.subscription_plan_id == 3
        @enterprise = true
      end
    end
  end
end
