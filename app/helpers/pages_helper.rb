module PagesHelper
  def file_size_validation
    @page = Page.friendly.find(params[:id])
    if @page.present?
      @user = @page.user
      if @user.user_uploaded_file_size_total.present?
        @current_size = @user.user_uploaded_file_size_total.total_size
      end
      if @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id == 1
        @limit = 50
        @check_mb = 300
        @mb = (300 * 1000000)
      elsif @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id == 2
        @limit = 100
        @check_mb = (2 * 1024)
        @mb = (2 * 1024 * 1000000)
      elsif @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id == 3
        @limit = 150
        @check_mb = (5 * 1024)
        @mb = (5 * 1024 * 1000000)
      elsif !@user.user_subscriptions.present?
        @limit = 50
        @check_mb = 50
        @mb = (50 * 1000000)
      end
      if @user.user_uploaded_file_size_total.present? && @user.user_uploaded_file_size_total.total_size.present? &&  @user.user_uploaded_file_size_total.total_size < @limit
        @dropzone = true
      else
        @dropzone = false
      end
    end
  end

  def page_validation
    if user_signed_in?
      @user = current_user
      if @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id == 1
        if @user.pages.present? && @user.pages.count < 1
          @allowed = true
        end
      elsif @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id == 2
        if @user.pages.present? && @user.pages.count <= 10
          @allowed = true
        elsif !@user.pages.present?
          @allowed = true
        end
      elsif @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id == 3
        @allowed = true
      elsif !@user.user_subscriptions.present?
        if @user.pages.present? && @user.pages.count < 1
          @allowed = true
        elsif !@user.pages.present?
          @allowed = true
        end
      end
    end
  end
  def decrease_total_size
    @google_infos = GoogleInfo.where(page_id: params[:id])
    @dropbox_infos = DropboxInfo.where(page_id: params[:id])
    @onedrive_infos = OnedriveInfo.where(page_id: params[:id])
    @all_records = @google_infos + @dropbox_infos + @onedrive_infos
    @size = 0.0
    @user = current_user
    if @user.user_uploaded_file_size_total.present?
      if @user.user_uploaded_file_size_total.total_size.present?
        @all_records.each do |i|
          @size = @size + i.size.to_f
        end
        user_total_size = @user.user_uploaded_file_size_total.total_size - @size
        @user = @user.user_uploaded_file_size_total.update(total_size: user_total_size)
      end
    end
  end
end
