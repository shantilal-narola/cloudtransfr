module DashboardHelper
  def update_user_uploaded_file_size_total
    @user = current_user
    if @user.user_uploaded_file_size_total.present? && @user.user_uploaded_file_size_total.plan_id.present? && @user.user_subscriptions.present?
      if @user.user_subscriptions.last.subscription_plan_id != @user.user_uploaded_file_size_total.plan_id
        if @user.user_uploaded_file_size_total.plan_id < @user.user_subscriptions.last.subscription_plan_id
          @user.user_uploaded_file_size_total.update_attributes(start_date: Date.today, end_date: Date.today + 1.month, plan_id: @user.user_subscriptions.last.subscription_plan_id )
        elsif @user.user_uploaded_file_size_total.plan_id > @user.user_subscriptions.last.subscription_plan_id
          @user.user_uploaded_file_size_total.update_attributes(start_date: Date.today, end_date: Date.today + 1.month, plan_id: @user.user_subscriptions.last.subscription_plan_id )
        end
      end
    elsif @user.user_uploaded_file_size_total.present? && !@user.user_uploaded_file_size_total.plan_id.present? && @user.user_subscriptions.present?
      @user.user_uploaded_file_size_total.update_attributes(start_date: Date.today, end_date: Date.today + 1.month, plan_id: @user.user_subscriptions.last.subscription_plan_id )
    end
  end

  def downgrade_user_plan
    @user = current_user
    if user_signed_in?
      plan_id = params[:plan_id]
      plan = Stripe::Plan.retrieve(plan_id)
      # Fetch subscription plan
      check_plan = SubscriptionPlan.find_by_plan_id(plan.id)
      if check_plan.id == 1
        @limit = 50
        @pages_limit = 1
      elsif check_plan.id == 2
        @limit = 100
        @pages_limit = 10
      elsif check_plan.id == 3
        @limit = 150
      end
      if @user.user_subscriptions.present? && @user.user_subscriptions.last.subscription_plan_id > check_plan.id
        if @user.user_uploaded_file_size_total.total_size > @limit || (@user.pages.present? && @user.pages.count > @pages_limit)
          flash[:error] = "You are using more space or you have more pages. You have not allowed to migrate this plan"
          redirect_to switch_plan_path
        end
      end
    end
  end

end
