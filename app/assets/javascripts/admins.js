// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require admins/jquery-2.2.3.min
//= require jquery_ujs
//= require turbolinks
//= require admins/bootstrap.min
//= require admins/datatables/jquery.dataTables.min
//= require admins/datatables/dataTables.bootstrap.min
//= require admins/datatables/dataTables.responsive.min
//= require admins/responsive.bootstrap.min
//= require admins/jquery.slimscroll.min
//= require admins/fastclick.min
//= require admins/app.min
//= require admins/demo
//= require formValidation.min
//= require formvalidation-bootstrap.min
