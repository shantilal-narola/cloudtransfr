class GoogleInfo < ApplicationRecord
  belongs_to :page
  after_save :update_total_size, only: [:upload]

  def update_total_size
    @user = self.page.user #self.current_user
    if @user.user_uploaded_file_size_total.present?
      if @user.user_uploaded_file_size_total.total_size.present?
        user_total_size = @user.user_uploaded_file_size_total.total_size + self.size.to_f
        @user = @user.user_uploaded_file_size_total.update(total_size: user_total_size)
      end
    end
  end

end
