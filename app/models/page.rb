class Page < ApplicationRecord
	extend FriendlyId
	validates_uniqueness_of :name, :message => "Already present"
	friendly_id :name, use: :slugged
  	belongs_to :user
  	has_many :dropbox_infos, :dependent => :delete_all
		has_many :google_infos, :dependent => :delete_all
		has_many :onedrive_infos, :dependent => :delete_all
		has_many :page_questions, :dependent => :delete_all
		has_one :page_cc_email, :dependent => :destroy

		
end
