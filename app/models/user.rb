class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_one :dropbox_detail, :dependent => :destroy
  has_one :google_detail, :dependent => :destroy
  has_one :onedrive_detail, :dependent => :destroy
  has_many :pages, :dependent => :destroy
  has_many :user_subscriptions, :dependent => :delete_all
  has_many :payment_decline_requests, :dependent => :delete_all
  has_one :user_uploaded_file_size_total, dependent: :destroy

  after_create :create_user_uploaded_file_size_total

  def create_user_uploaded_file_size_total
    @user_uploaded_file_size_total = UserUploadedFileSizeTotal.new(user_id: self.id, start_date: self.created_at, end_date: self.created_at + 7.days, total_size: 0.0 )
    @user_uploaded_file_size_total.save
  end
end
