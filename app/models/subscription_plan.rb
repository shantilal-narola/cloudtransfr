class SubscriptionPlan < ApplicationRecord
  has_many :user_subscriptions, :dependent => :delete_all
  has_many :payment_decline_requests, :dependent => :delete_all
end
