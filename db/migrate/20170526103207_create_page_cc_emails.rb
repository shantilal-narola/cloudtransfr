class CreatePageCcEmails < ActiveRecord::Migration[5.0]
  def change
    create_table :page_cc_emails do |t|
      t.references :page, foreign_key: true
      t.string :cc_email

      t.timestamps
    end
  end
end
