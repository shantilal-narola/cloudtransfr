class RenameGoogleDriveColumns < ActiveRecord::Migration[5.0]
  def change
    rename_column :google_details, :uid, :code
    rename_column :google_details, :account_id, :refresh_token

    rename_column :google_infos, :revision, :file_id
    change_column :google_infos, :file_id, :string
    rename_column :google_infos, :rev, :folder_id
  end
end
