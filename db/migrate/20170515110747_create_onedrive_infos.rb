class CreateOnedriveInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :onedrive_infos do |t|
      t.references :page, foreign_key: true
      t.integer :revision
      t.string :rev
      t.timestamp :modified
      t.string :mine_type
      t.string :path
      t.string :size
      t.timestamp :client_mtime

      t.timestamps
    end
  end
end
