class AddPlanIdToUserUploadedFileSizeTotal < ActiveRecord::Migration[5.0]
  def change
    add_column :user_uploaded_file_size_totals, :plan_id, :integer
  end
end
