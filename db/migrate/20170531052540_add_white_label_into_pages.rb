class AddWhiteLabelIntoPages < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :white_label_title, :string
  end
end
