class CreatePaymentDeclineRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_decline_requests do |t|
      t.references :user, foreign_key: true
      t.references :subscription_plan, foreign_key: true
      t.text :comment
      t.boolean :is_approved, default: false

      t.timestamps
    end
  end
end
