class AddFilenameIntoGoogledrive < ActiveRecord::Migration[5.0]
  def change
    add_column :dropbox_infos, :uploaded_filename, :string
    add_column :google_infos, :original_filename, :string
    add_column :google_infos, :uploaded_filename, :string

    add_column :onedrive_infos, :original_filename, :string
    add_column :onedrive_infos, :uploaded_filename, :string
  end
end
