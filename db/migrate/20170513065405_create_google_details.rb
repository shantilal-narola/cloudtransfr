class CreateGoogleDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :google_details do |t|
      t.references :user, foreign_key: true
      t.string :access_token
      t.string :token_type
      t.string :uid
      t.string :account_id
      t.boolean :is_active

      t.timestamps
    end
  end
end
