class AddExpiryDateIntoGoogleDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :google_details, :expires_at, :timestamp
    add_column :google_details, :expire, :integer
  end
end
