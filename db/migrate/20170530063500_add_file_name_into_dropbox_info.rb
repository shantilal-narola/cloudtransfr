class AddFileNameIntoDropboxInfo < ActiveRecord::Migration[5.0]
  def change
    add_column :dropbox_infos, :original_filename, :string
  end
end
