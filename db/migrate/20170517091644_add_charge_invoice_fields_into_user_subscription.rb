class AddChargeInvoiceFieldsIntoUserSubscription < ActiveRecord::Migration[5.0]
  def change
    add_column :user_subscriptions, :stripe_charge_id, :string
    add_column :user_subscriptions, :stripe_invoice_id, :string
  end
end
