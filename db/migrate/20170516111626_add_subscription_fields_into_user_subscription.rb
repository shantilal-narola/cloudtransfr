class AddSubscriptionFieldsIntoUserSubscription < ActiveRecord::Migration[5.0]
  def change
    add_column :user_subscriptions, :stripe_subscription_id, :string
    add_column :user_subscriptions, :stripe_customer_id, :string
    add_column :user_subscriptions, :stripe_payment_status, :string
    add_column :user_subscriptions, :subscription_start_priod, :timestamp
    add_column :user_subscriptions, :subscription_end_priod, :timestamp
    add_column :user_subscriptions, :livemode, :boolean
  end
end
