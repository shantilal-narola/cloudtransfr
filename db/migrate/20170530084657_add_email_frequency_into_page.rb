class AddEmailFrequencyIntoPage < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :email_frequency, :boolean, default: true
  end
end
