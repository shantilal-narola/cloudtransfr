class CreateSubscriptionPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :subscription_plans do |t|
      t.string :name
      t.string :plan_id
      t.string :interval
      t.integer :amount

      t.timestamps
    end
  end
end
