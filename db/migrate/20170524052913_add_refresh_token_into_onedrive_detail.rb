class AddRefreshTokenIntoOnedriveDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :onedrive_details, :refresh_token, :text
    add_column :onedrive_details, :expires_in, :integer
    add_column :onedrive_details, :expires_at, :timestamp
  end
end
