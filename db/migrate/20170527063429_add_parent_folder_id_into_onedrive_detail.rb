class AddParentFolderIdIntoOnedriveDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :onedrive_details, :app_folder_id, :string
    add_column :onedrive_details, :cloud_folder_id, :string
  end
end
