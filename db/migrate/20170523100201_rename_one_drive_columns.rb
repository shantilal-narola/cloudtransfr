class RenameOneDriveColumns < ActiveRecord::Migration[5.0]
  def change
    rename_column :onedrive_infos, :revision, :file_id
    change_column :onedrive_infos, :file_id, :string
    rename_column :onedrive_infos, :rev, :source

  end
end
