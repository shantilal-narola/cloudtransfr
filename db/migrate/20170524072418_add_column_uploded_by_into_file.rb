class AddColumnUplodedByIntoFile < ActiveRecord::Migration[5.0]
  def change
    add_column :google_infos, :uploaded_by, :string
    add_column :dropbox_infos, :uploaded_by, :string
    add_column :onedrive_infos, :uploaded_by, :string
  end
end
