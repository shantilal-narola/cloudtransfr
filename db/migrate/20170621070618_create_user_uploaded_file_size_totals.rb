class CreateUserUploadedFileSizeTotals < ActiveRecord::Migration[5.0]
  def change
    create_table :user_uploaded_file_size_totals do |t|
      t.references :user, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.float :total_size

      t.timestamps
    end
  end
end
