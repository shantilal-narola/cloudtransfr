class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :name
      t.references :user, foreign_key: true
      t.boolean :is_active
      t.integer :cloud_storage_id
      t.string :cloud_storage_type
      t.string :slug

      t.timestamps
    end
    add_index :pages, :slug, unique: true
  end
end
