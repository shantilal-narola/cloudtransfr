class CreatePageQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :page_questions do |t|
      t.references :page, foreign_key: true
      t.string :question
      t.string :question_type
      t.text :description
      t.boolean :is_required

      t.timestamps
    end
  end
end
