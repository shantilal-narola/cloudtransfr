class AddFolderIdIntoPage < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :folder_id, :string
  end
end
