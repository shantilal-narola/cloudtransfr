# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
  SubscriptionPlan.create(name: "Starter", plan_id: "82a9e066-333e-4edd-8254-1957e3a9af84", interval: "year", amount: 24)
  SubscriptionPlan.create(name: "Advanced", plan_id: "a10a65a0-32ba-4bb9-bc42-9a74d4ee50ef", interval: "year", amount: 96)
  SubscriptionPlan.create(name: "Enterprise", plan_id: "25400a82-7de6-42b0-bc06-6d6653dd066f", interval: "year", amount: 480)
