Rails.application.routes.draw do
  namespace :admins do
    get 'pages', to: "pages#index", as: :pages
    get 'documents/:id', to: "documents#index", as: :documents
    get 'users/:id', to: "users#show", as: :show_user
    get 'users/:id/edit', to: "users#edit", as: :edit_user
    put 'users/:id/update', to: "users#update", as: :update_user
    delete 'user/:id', to: "users#destroy", as: :user_delete
    delete 'page/:id', to: "pages#destroy", as: :page_delete
    get 'decline_request', to: "users#decline_request", as: :decline_request
    put 'decline_request/:id', to: "users#update_request_status", as: :update_request_status
    delete 'decline_request/:id', to: "users#destroy_request", as: :decline_request_delete
  end

  authenticated :admin do
    root :to => "admins/users#index"
  end

  authenticated :user do
    root :to => "dashboard#index"
  end

  devise_for :admins, controllers: { registrations: "admins/registrations",sessions: "admins/sessions", confirmations: "admins/confirmations", passwords: "admins/passwords", unlocks: "admins/unlocks" }
  get 'subscription',to: 'subscriptions#idnex'
  post 'subscriptions', to: 'subscriptions#create'
  get 'authenticates/index'

  devise_for :users, controllers: { registrations: "registrations" }
  post 'check_email', to: 'users#check_email'
  post 'check_page', to: 'users#check_page'
  post 'is_email_exist', to: 'users#is_email_exist'
  get 'staticpage/index'
  get 'staticpage/test'

  get 'staticpage/dropbox'
  get 'staticpage/googledrive'
  get 'staticpage/onedrive'
  get 'staticpage/googleapidrive'

  root to: "dashboard#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'users/:id/edit', to: 'users#edit', as: :edit_user
  put 'users/:id', to: 'users#update', as: :update_user
  get 'dashboard/privacy_policy', to: 'dashboard#privacy_policy', as: :privacy_policy
  get 'dashboard/switch_plan', to: 'dashboard#switch_plan', as: :switch_plan
  get 'dashboard/addcard/:plan_id', to: 'dashboard#addcard', as: :addcard
  post 'subscription_checkout', to: 'dashboard#subscription_checkout', as: :subscription_checkout
  post 'payment_decline_request', to: 'dashboard#payment_decline_request', as: :payment_decline_request

  resources :pages do
  	collection do
  		post 'upload'
      get ':type', to: 'pages#new_page',as: :new_page
      post 'page_exist', to: 'pages#page_exist', as: :page_exist
      put 'pages/:id', to: 'pages#update'
  	end
  end
  post 'add_question_setting/:id', to: 'pages#add_question', as: :add_question
  get 'remove_question/:id', to: 'pages#remove_question', as: :remove_question
  post 'add_question_answer', to: 'pages#add_question_answer', as: :add_question_answer
  get ':id', to: 'pages#show_page',as: :show_page
  get ':id/dropbox_download' => 'pages#dropbox_download', as: 'dropbox_download'
  delete ':id/remove_from_dropbox' => 'pages#remove_from_dropbox', as: :remove_from_dropbox
  get ':id/googledrive_download', to: 'pages#googledrive_download', as: :googledrive_download
  delete ':id/remove_from_googledrive' => 'pages#remove_from_googledrive', as: :remove_from_googledrive
  get ':id/onedrive_download', to: 'pages#onedrive_download', as: :onedrive_download
  delete ':id/remove_from_onedrive' => 'pages#remove_from_onedrive', as: :remove_from_onedrive


  match "*path" => redirect("/") , via: [:get, :post]
end
